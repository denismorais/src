import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TelaCarregamentoPageRoutingModule } from './tela-carregamento-routing.module';

import { TelaCarregamentoPage } from './tela-carregamento.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TelaCarregamentoPageRoutingModule
  ],
  declarations: [TelaCarregamentoPage]
})
export class TelaCarregamentoPageModule {}
