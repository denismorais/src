import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TelaCarregamentoPage } from './tela-carregamento.page';

describe('TelaCarregamentoPage', () => {
  let component: TelaCarregamentoPage;
  let fixture: ComponentFixture<TelaCarregamentoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaCarregamentoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TelaCarregamentoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
