import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TelaCarregamentoPage } from './tela-carregamento.page';

const routes: Routes = [
  {
    path: '',
    component: TelaCarregamentoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TelaCarregamentoPageRoutingModule {}
