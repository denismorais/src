import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TelaACliPageRoutingModule } from './tela-acli-routing.module';

import { TelaACliPage } from './tela-acli.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TelaACliPageRoutingModule
  ],
  declarations: [TelaACliPage]
})
export class TelaACliPageModule {}
