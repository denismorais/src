import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TelaACliPage } from './tela-acli.page';

const routes: Routes = [
  {
    path: '',
    component: TelaACliPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TelaACliPageRoutingModule {}
