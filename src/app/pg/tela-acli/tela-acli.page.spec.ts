import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TelaACliPage } from './tela-acli.page';

describe('TelaACliPage', () => {
  let component: TelaACliPage;
  let fixture: ComponentFixture<TelaACliPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaACliPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TelaACliPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
