import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
  },
  {
    path: 'tela-carregamento',
    loadChildren: () => import('./pg/tela-carregamento/tela-carregamento.module').then( m => m.TelaCarregamentoPageModule)
  },
  {
    path: 'tela-login',
    loadChildren: () => import('./pg/tela-login/tela-login.module').then( m => m.TelaLoginPageModule)
  },
  {
    path: 'sobre',
    loadChildren: () => import('./pg/sobre/sobre.module').then( m => m.SobrePageModule)
  },
  {
    path: 'agenda',
    loadChildren: () => import('./pg/agenda/agenda.module').then( m => m.AgendaPageModule)
  },
  {
    path: 'tela-acli',
    loadChildren: () => import('./pg/tela-acli/tela-acli.module').then( m => m.TelaACliPageModule)
  },
  {
    path: 'agendamento',
    loadChildren: () => import('./pg/agendamento/agendamento.module').then( m => m.AgendamentoPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
